package com.devcamp.bookauthorapi;


import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin   //Java @CrossOrigin: cho phép CORS trên RESTful web service.
@RestController
public class RestAPI_book_author {  

    //http://localhost:8080/books
    @GetMapping("/books")
    public ArrayList<Book> splitString() {

        // khởi tạo 3 đối tượng tác giả 
        Author tac_gia1 = new Author("thuong", "thuong@gamil.com", 'f'); // 'm'  of 'f'
        Author tac_gia2 = new Author("linh", "linh@gamil.com", 'f');
        Author tac_gia3 = new Author("duong", "duong@gamil.com", 'm');

        // khởi tạo 3 đối tượng sách 
        Book book1 = new Book("toan",tac_gia1, 1000);
        Book book2 = new Book("ly",tac_gia2, 1000);
        Book book3 = new Book("hoa",tac_gia3, 1000);

        System.out.println(tac_gia1);
        System.out.println(tac_gia2);
        System.out.println(tac_gia3);
        ArrayList<Book> listString = new ArrayList<Book>();
        // thêm sách vào arr list
        listString.add(book1);
        listString.add(book2);
        listString.add(book3);
        
        // trả về là arr list
        return listString;
    }

    
}
